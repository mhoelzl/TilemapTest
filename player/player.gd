extends KinematicBody2D

const SPEED = 250

var nav_system
export(Vector2) var nav_start = null
export(Vector2) var nav_target = null
var nav_path = []
var current_path_index = 0

func clear_navigation():
	nav_start = null
	nav_target = null
	nav_path = []
	current_path_index = 0

func _input(event):
	if event.is_action_pressed("pick_target"):
		nav_start = position
		nav_target = event.position
		if nav_system:
			nav_path = nav_system.get_simple_path(nav_start, nav_target, true)
		else:
			nav_path = PoolVector2Array([nav_start, nav_target])
		current_path_index = 0
		# print("Navigation path: %s" % nav_path)
	elif event.is_action_pressed("manual_navigation"):
		clear_navigation()

func compute_direction_from_movement_input():
	var direction = Vector2()
	
	if Input.is_action_pressed("move_left"):
		direction.x -= 1
	if Input.is_action_pressed("move_right"):
		direction.x += 1
	if Input.is_action_pressed("move_up"):
		direction.y -= 1
	if Input.is_action_pressed("move_down"):
		direction.y += 1
	return direction.normalized()
	
	

func compute_direction_from_nav_path(delta):
	if len(nav_path) >= 2 + current_path_index:
		var segment_end = nav_path[current_path_index + 1]
		# print("Position: %s, End: %s, Length: %f" % [position, segment_end, position.distance_to(segment_end)])
		if position.distance_to(segment_end) < 2 * delta * SPEED:
			current_path_index += 1
			return compute_direction_from_nav_path(delta)
		var segment_start = position # nav_path[current_path_index]
		var segment_vector = segment_end - segment_start
		return segment_vector.normalized()
	else:
		clear_navigation()		
		return Vector2()


func _physics_process(delta):
	
	var direction
	if len(nav_path) < 2:
		direction = compute_direction_from_movement_input()
	else:
		direction = compute_direction_from_nav_path(delta)
	move_and_slide(direction * SPEED)
