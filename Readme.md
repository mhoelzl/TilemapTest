Tests for Godot Tile Maps
=========================

I wrote the following tests while preparing [this pull request](https://github.com/godotengine/godot/pull/13216). They mostly check whether tilesets with different offsets are displayed correctly, and whether collision and navigation works.

- The `uv_test_*` scenes show all possible combinations of the `transpose`, `flip_h`, and `flip_v` flags for a tile taken from a UV map, so that it's easy to verify its orientation.  Each scene uses one of the top-left, center or bottom-left settings for origin. They should look exactly the same.

- The `brick_test_*` scenes show all those combinations for tiles with collision shapes. Obviously, the collision shape should match the brick walls.

- The `nav_test_*` scenes use a few road tiles with attached collision and navigation. The "player" (i.e., the little circle) should be able to navigate on the roads, but not in the grass. The file `nav_test_all.tscn` holds a scene in which three tile maps with top-left, center and bottom-left settings are placed next to each other. The player should be able to navigate smoothly between the tiles.

![Image of `nav_test_all`](nav_test_all.gif)